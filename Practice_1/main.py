import random

score = 0

problems_tried = 0

correct_answers = 0

game = True

user_name = ""

load = input("Load last game? ")

if load == "yes":
    file = open("save.txt", 'r')
    user_name = str(file.readline())
    score = int(file.readline())
    problems_tried = int(file.readline())
    correct_answers = int(file.readline())
    print("Welcome "+user_name)
    print("Last game you had:")
    print("A score of "+str(score))
    print(str(problems_tried)+" problems tried")
    print(str(correct_answers)+" correct answers")
    print("Let's continue!")
else:
    user_name = input("What is your name? ")
    print("Welcome "+user_name+"!")


while game == True:
    firstNumber = random.randint(0, 100)
    secondNumber = random.randint(0, 100)

    operation = random.randint(0, 2)
    if operation == 0:
        print("What is "+str(firstNumber)+" + "+str(secondNumber)+"?")
        solution = firstNumber + secondNumber
    if operation == 1:
            print("What is "+str(firstNumber)+" - "+str(secondNumber)+"?")
            solution = firstNumber - secondNumber
    if operation == 2:
            print("What is "+str(firstNumber)+" * "+str(secondNumber)+"?")
            solution = firstNumber * secondNumber

    print("Answer: ")
    user_answer = input("")

    if user_answer == str(solution):
        print("Correct!")
        print("You get 2 points")
        score+=2
        problems_tried+=1
        correct_answers+=1
    elif user_answer == "" or user_answer == "bye":
        print("Bye "+user_name+"! Until next time!")
        game = False
    else:
        print("Incorrect... The correct answer is : "+str(solution)+".")
        print("You lose 1 point...")
        score-=1
        problems_tried+=1

    print("Current score: "+str(score))
    print(" ")

    file = open("save.txt", 'w')
    file.write(user_name + "\n")
    file.write(str(score) + "\n")
    file.write(str(problems_tried) + "\n")
    file.write(str(correct_answers) + "\n")


