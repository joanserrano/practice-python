import pygame

class Bullet(pygame.sprite.Sprite):
    """Class to manage bullets fired from the ship"""
    def __init__(self, settings, screen, ship):
        """Create a bullet at the ship's position"""
        super(Bullet, self).__init__()
        self.screen = screen
        self.rect = pygame.Rect(ship.rect.centerx, ship.rect.top, settings.bullet_width, settings.bullet_height)

    def update(self):
        self.rect.centery -= 1

    def draw(self):
        pygame.draw.rect(self.screen, (0, 255, 0), self.rect )
